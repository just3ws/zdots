# Zdots

My Zsh dotfiles.

## Installation

```shell
mv -f .zshenv .zshenv.bak
mkdir -p ~/.local/share/zsh
cd
git clone git@github.com:just3ws/zdots.git ~/.config/zsh`
ln ~/.config/zsh/.zshenv
exec "$SHELL"
```

## Zsh Bookmarks

### Official

- [Mirror of the Z shell source code repository](https://github.com/zsh-users/zsh)
- [THE ZSH FAQ](http://www.zsh.org/FAQ/)
- [THE ZSH WEB PAGE](http://www.zsh.org/)
- [THE ZSH USERGUIDE](http://zsh.sourceforge.net/Guide/)
- [THE ZSH WIKI](http://www.zshwiki.org/)

### Homepage

- [Zsh](http://www.zsh.org/)

### Documentation

- [A User's Guide to the Z-Shell](http://zsh.sourceforge.net/Guide/zshguide.html)
- [A User's Guide to ZSH](http://zsh.sourceforge.net/Guide/)
- [Table of Contents](http://zsh.sourceforge.net/Intro/intro_toc.html)
- [Filename Generation](http://zsh.sourceforge.net/Intro/intro_2.html)
- [A User's Guide to the Z-Shell](http://zsh.sourceforge.net/Guide/zshguide02.html)
- [Startup Files](http://zsh.sourceforge.net/Intro/intro_3.html)
- [Z-Shell Frequently-Asked Questions](http://zsh.sourceforge.net/FAQ/)
- [ZSH - Documentation](http://zsh.sourceforge.net/Doc/)
- [ZSH - Reference Card](http://zsh.sourceforge.net/Refcard/)
- [ZSH - THE Z SHELL](http://zsh.sourceforge.net/)
- [zsh - Browse Files at SourceForge.net](https://sourceforge.net/projects/zsh/files/)
- [zsh: 12 Conditional Expressions](http://zsh.sourceforge.net/Doc/Release/Conditional-Expressions.html)
- [zsh: 6 Shell Grammar](http://zsh.sourceforge.net/Doc/Release/Shell-Grammar.html#Alternate-Forms-For-Complex-Commands)
- [zsh: Table of Contents](http://zsh.sourceforge.net/Doc/Release/zsh_toc.html)

### ZshWiki

- [ZshWiki - examples:aliasdirs](http://zshwiki.org/home/examples/aliasdirs)
- [ZshWiki - options:history](http://zshwiki.org/home/options/history)
- [ZshWiki](http://zshwiki.org/home/)
- [ZshWiki - tutorials](http://zshwiki.org/home/tutorials)

### Others

- [.zshrc (ZSH lazy load) · GitHub](https://gist.github.com/QinMing/364774610afc0e06cc223b467abe83c0)
- [Ask HN: Share your favourite bash/zsh aliases | Hacker News](https://news.ycombinator.com/item?id=9869231)
- [From Bash to Z Shell: Conquering the Command Line](http://www.bash2zsh.com/)
- [GitHub - psprint/zsh-navigation-tools: Curses-based tools for Zsh, e.g. multi-word history searcher](https://github.com/psprint/zsh-navigation-tools)
- [Master Your Z Shell with These Outrageously Useful Tips - Blog - Reason I Am Here - Nacho Caballero](http://reasoniamhere.com/2014/01/11/outrageously-useful-tips-to-master-your-z-shell/)
- [Mastering the path_helper utility of MacOSX (DevelopersCorner.MasteringThePathHelper) - XWiki](http://www.softec.lu/site/DevelopersCorner/MasteringThePathHelper)
- [Micah Elliott: How to Structure a .zshrc](http://micahelliott.com/posts/2016-01-16-how-to-structure-zshrc.html)
- [My .zshrc file](https://gist.github.com/zanshin/1142739)
- [No, really. Use Zsh. - IFHO](http://fendrich.se/blog/2012/09/28/no/)
- [One shell to rule them all... - (think)](http://batsov.com/articles/2011/04/29/one-shell-to-rule-them-all/)
- [Profiling ZSH startup time | Kevin Burke](https://kev.inburke.com/kevin/profiling-zsh-startup-time/)
- [Profiling zsh shell scripts — Xebia Blog](http://blog.xebia.com/profiling-zsh-shell-scripts/)
- [Search · topic:zsh · GitHub](https://github.com/search?q=topic%3Azsh&type=Repositories)
- [Search · topic:zsh-configuration · GitHub](https://github.com/search?q=topic%3Azsh-configuration&type=Repositories)
- [Search · topic:zshrc · GitHub](https://github.com/search?q=topic%3Azshrc&type=Repositories)
- [Speed up zsh compinit by only checking cache once a day. · GitHub](https://gist.github.com/ctechols/ca1035271ad134841284)
- [Speeding up my ZSH load | Carlos Alexandro Becker](https://carlosbecker.com/posts/speeding-up-zsh/)
- [Updating Your Shell with Homebrew | John D. Jameson](https://johndjameson.com/blog/updating-your-shell-with-homebrew/)
- [Use Homebrew zsh Instead of the OS X Default : Rick Cogley Central](https://rick.cogley.info/post/use-homebrew-zsh-instead-of-the-osx-default/)
- [What does `zstyle` do?](https://unix.stackexchange.com/questions/214657/what-does-zstyle-do)
- [Why Zsh is Cooler than Your Shell](https://www.slideshare.net/jaguardesignstudio/why-zsh-is-cooler-than-your-shell-16194692)
- [Writing own completion functions](https://askql.wordpress.com/2011/01/11/zsh-writing-own-completion/)
- [Z shell - Wikipedia](https://en.wikipedia.org/wiki/Z_shell)
- [Z shell made easy | TuxRadar Linux](http://www.tuxradar.com/content/z-shell-made-easy)
- [ZSH-LOVERS(1)](https://grml.org/zsh/zsh-lovers.html)
- [Zsh Configuration From the Ground Up - Zanshin.net](http://zanshin.net/2013/02/02/zsh-configuration-from-the-ground-up/)
- [Zsh Tips, Tricks and Examples by zzapper](http://www.rayninfo.co.uk/tips/zshtips.html)
- [Zsh Workshop: Table of Contents](https://www-s.acm.illinois.edu/workshops/zsh/toc.html)
- [Zsh/Bash startup files loading order (.bashrc, .zshrc etc.) | The Lumber Room](https://shreevatsa.wordpress.com/2008/03/30/zshbash-startup-files-loading-order-bashrc-zshrc-etc/)
- [chris blogs: 10 new zsh tricks you may not know...](http://chneukirchen.org/blog/archive/2012/02/10-new-zsh-tricks-you-may-not-know.html)
- [molovo/zlint: A linter and code style checker for ZSH](https://github.com/molovo/zlint)
- [per directory zsh config (Example)](https://coderwall.com/p/a3xreg/per-directory-zsh-config)
- [reload all running zsh instances - Super User](https://superuser.com/questions/852912/reload-all-running-zsh-instances)
- [the8/terminal-app.zsh: OSX 10.11(El Capitan) Terminal.app and Zsh integration using new status escape codes and a dash of Emoji to make your terminal shine.](https://github.com/the8/terminal-app.zsh)
- [zsh-syntax-highlighting/INSTALL.md at master · zsh-users/zsh-syntax-highlighting · GitHub](https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md)

### StackExchange

- [Functions digest is NOT treated the same as a directory of function files!](https://unix.stackexchange.com/questions/306122/functions-digest-is-not-treated-the-same-as-a-directory-of-function-files)
- [How to define and load your own shell function in zsh - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/33255/how-to-define-and-load-your-own-shell-function-in-zsh)
- [shell - Why zsh and ksh93 chose to be non-compliant in pattern matching? - Unix & Linux Stack Exchange](http://unix.stackexchange.com/questions/265431/why-zsh-and-ksh93-chose-to-be-non-compliant-in-pattern-matching/265446)

### zsh-users

- [zsh-users · GitHub](https://github.com/zsh-users)
- [zsh-users/zsh: Mirror of the Z shell source code repository.](https://github.com/zsh-users/zsh)
- [zsh-users](https://github.com/zsh-users)
- [GitHub - zsh-users/zsh: Mirror of the Z shell source code repository.](https://github.com/zsh-users/zsh)

### dotfiles

- [GitHub - stephenmckinney/dotfiles: Config files for ack, gem, git, irb, iterm2, pry, rails, rspec, rvm, tmux, zsh with a focus on Ruby and Rails development. Solarized ALL the things.](https://github.com/stephenmckinney/dotfiles)
- [dotfiles/config.zsh at master · Drakirus/dotfiles · GitHub](https://github.com/Drakirus/dotfiles/blob/master/zsh/config.zsh#L7)
- [http://pages.cs.wisc.edu/~arumuga/dotfiles/.zshrc](http://pages.cs.wisc.edu/~arumuga/dotfiles/.zshrc)
- [ryanb/dotfiles: config files for zsh, bash, completions, gem, git, irb, rails](https://github.com/ryanb/dotfiles)
- [spicycode/ze-best-zsh-config: Ze Best ZSH Config](https://github.com/spicycode/ze-best-zsh-config)
- [voku/dotfiles: dotfiles for Bash / ZSH / Git Bash (Windows) / Cygwin (Windows) / Bash on Ubuntu on Windows](https://github.com/voku/dotfiles)
- [GitHub - dotphiles/dotzsh: A community driven framework for zsh](https://github.com/dotphiles/dotzsh)
- [.zsh/aliases at master · robertklep/.zsh](https://github.com/robertklep/.zsh/blob/master/aliases)


### Frameworks

- [Eriner/zim: ZIM - Zsh IMproved](https://github.com/Eriner/zim)
- [GitHub - sorin-ionescu/prezto: The configuration framework for Zsh](https://github.com/sorin-ionescu/prezto)
- [GitHub - zimframework/zim: ZIM - Zsh IMproved](https://github.com/zimframework/zim)
- [How Zim Works (part 1) – Eriner](https://eriner.me/2015/12/29/How-Zim-Works-pt-1/)
- [Migrating from Prezto to Zim (Zsh IMproved) · Linuxious](https://linuxious.com/2015/12/26/migrating-from-prezto-to-zim-zsh-improved/)
- [Troubleshooting · robbyrussell/oh-my-zsh Wiki](https://github.com/robbyrussell/oh-my-zsh/wiki/Troubleshooting)
- [Why Oh My Zsh is completely broken](https://archive.zhimingwang.org/blog/2015-05-03-why-oh-my-zsh-is-completely-broken.html)
- [prezto/runcoms at master · sorin-ionescu/prezto](https://github.com/sorin-ionescu/prezto/tree/master/runcoms)
- [The “Nano-Framework” For Adding Scripts To Your .{bash/zsh}rc Files](https://hackernoon.com/the-nano-framework-for-adding-scripts-to-your-bash-zsh-rc-files-df2732e3463#.3zhvolv8t)

### Prompts

- [Customize your zsh prompt](http://richard-zhao.com/blog/2015/08/26/zsh-prompt/)
- [How to Customize Your Command Prompt](https://code.tutsplus.com/tutorials/how-to-customize-your-command-prompt--net-24083)
- [My Extravagant Zsh Prompt](http://stevelosh.com/blog/2010/02/my-extravagant-zsh-prompt/)
- [Prompt Expansion](http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html)
- [Prompting](http://zsh.sourceforge.net/Intro/intro_14.html)
- [arialdomartini/oh-my-git: An opinionated git prompt for bash and zsh](https://github.com/arialdomartini/oh-my-git)
- [sindresorhus/pure: Pretty, minimal and fast ZSH prompt](https://github.com/sindresorhus/pure)

### vcs_info

- [Customize your ZSH prompt with vcs_info](http://arjanvandergaag.nl/blog/customize-zsh-prompt-with-vcs-info.html)
- [GIT INFO IN YOUR ZSH PROMPT](http://eseth.org/2010/git-in-zsh.html)
- [Git info in your ZSH Prompt](http://briancarper.net/blog/570.html)
- [My very own Zsh prompt](http://matija.suklje.name/my-very-own-zsh-prompt)
- [vcs_info-examples](https://github.com/zsh-users/zsh/blob/master/Misc/vcs_info-examples)

### Async

- [zsh-async](https://github.com/mafredri/zsh-async)
- [Asynchronous RPROMPT?](https://unix.stackexchange.com/questions/91087/asynchronous-rprompt)
- [An Asynchronous Shell Prompt](http://www.anishathalye.com/2015/02/07/an-asynchronous-shell-prompt/)

### unsorted
- http://zshwiki.org/home/zle/vi-mode
- http://zshwiki.org/home/zle/bindkeys#why_isn_t_control-r_working_anymore
